package pl.regzand.neuralnetwork;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.Scanner;

import pl.regzand.utils.Random;

public class NeuralNetwork {
	
	public static final double LEARNING_RATIO = 0.2;
	public static final double ALLOWED_ERROR = 0.2;
	
	public final int[] layers;
	
	public final double[][][]	weights;
	public final double[][]		biases;
	public final double[][]		outputs;
	public final double[][]		inputs;
	public final double[][]		errors;
	public final double[][][]	gradientWeights;
	public double[][]		gradientBiases;
	
	/**
	 * Creates new neural network with random weights and biases
	 * @param int[] layers - array of count of neurons in each layer
	 */
	public NeuralNetwork(int[] layers){
		this.layers = layers;
		
		// resizing all arrays
		weights			= new double[layers.length][][];
		biases			= new double[layers.length][];
		outputs			= new double[layers.length][];
		inputs			= new double[layers.length][];
		errors			= new double[layers.length][];
		gradientWeights	= new double[layers.length][][];
		gradientBiases	= new double[layers.length][];
		
		// resizing all layers
		for(int i = 0; i<layers.length; i++){
			
			weights[i]			= new double[layers[i]][];
			biases[i]			= new double[layers[i]];
			outputs[i]			= new double[layers[i]];
			inputs[i]			= new double[layers[i]];
			errors[i]			= new double[layers[i]];
			gradientWeights[i]	= new double[layers[i]][];
			gradientBiases[i]	= new double[layers[i]];
			
		}
		
		// creating all layers (except input layer)
		for(int i = 1; i<layers.length; i++){
			
			for(int j = 0; j<layers[i]; j++){
				
				weights[i][j]			= new double[layers[i-1]];
				gradientWeights[i][j]	= new double[layers[i-1]];
				
				// setting random weights
				for(int k = 0; k<layers[i-1]; k++){
					weights[i][j][k] = Random.getDouble(0, 0.5)+Random.getDouble(-0.5, 0);
				}
				
				// setting random bias
				biases[i][j] = Random.getDouble(0, 0.5)+Random.getDouble(-0.5, 0);
			}
			
		}
	}
	
	/**
	 * Loads neural network from given file
	 * @param File file - text file with neural network data
	 * @throws FileNotFoundException 
	 */
	public NeuralNetwork(File file) throws FileNotFoundException{
		Scanner scanner = new Scanner(file).useLocale(Locale.US);
		
		// number of layers
		layers = new int[scanner.nextInt()];
		
		// resizing all arrays
		weights			= new double[layers.length][][];
		biases			= new double[layers.length][];
		outputs			= new double[layers.length][];
		inputs			= new double[layers.length][];
		errors			= new double[layers.length][];
		gradientWeights	= new double[layers.length][][];
		gradientBiases	= new double[layers.length][];
		
		// resizing all layers
		for(int i = 0; i<layers.length; i++){
			
			// number of neurons in each layer
			layers[i] = scanner.nextInt();
			
			weights[i]			= new double[layers[i]][];
			biases[i]			= new double[layers[i]];
			outputs[i]			= new double[layers[i]];
			inputs[i]			= new double[layers[i]];
			errors[i]			= new double[layers[i]];
			gradientWeights[i]	= new double[layers[i]][];
			gradientBiases[i]	= new double[layers[i]];
			
		}
		
		// loading all layers (except input layer)
		for(int i = 1; i<layers.length; i++){
			
			for(int j = 0; j<layers[i]; j++){
				
				weights[i][j]			= new double[layers[i-1]];
				gradientWeights[i][j]	= new double[layers[i-1]];
				
				// loading weights
				for(int k = 0; k<layers[i-1]; k++){
					weights[i][j][k] = scanner.nextDouble();
				}
				
				// loading bias
				biases[i][j] = scanner.nextDouble();
			}
			
		}
		
		scanner.close();
	}
	
	/**
	 * Saves network structure to given file
	 * @param file
	 * @throws FileNotFoundException 
	 */
	public void saveToFile(File file) throws FileNotFoundException{
		PrintWriter print = new PrintWriter(file);

		print.println(layers.length);
		
		for(int count : layers){
			print.print(count+" ");
		}
		print.println();
		
		for(int i = 1; i<layers.length; i++){
			for(int j = 0; j<layers[i]; j++){
				for(int k = 0; k<layers[i-1]; k++)
					print.print(weights[i][j][k]+" ");
				print.println(biases[i][j]);
			}
		}
		
		print.close();
	}
	
	public void teach(double[][] inputs, double[][] expected){
		if(inputs.length != expected.length)
			throw new IllegalArgumentException("Arguments inputs and expected has to be thisame length");
		
		// creating list of order
		ArrayList<Integer> order = new ArrayList<Integer>();
		for(int i = 0; i<inputs.length; i++)
			order.add(i);
		
		// teaching as long as there are errors
		boolean learned = false;
		while(!learned){
			learned = true;
			
			// creating random order
			Collections.shuffle(order);
			
			// teaching each test case form input
			for(int i = 0; i<inputs.length; i++){
				int index = order.get(i);
				
				// teaching
				this.teachOnce(inputs[index], expected[index]);
				
				// checking answers
				for(int j = 0; j<layers[layers.length-1]; j++){
					if(Math.abs(expected[index][j] - outputs[layers.length-1][j]) > ALLOWED_ERROR){
						learned = false;
						break;
					}
				}
			}
		}
	}
	
	/**
	 * Performs calculations on network using given input (first layer) and returns result (last layer)
	 * @param input
	 * @return double[]
	 */
	public double[] calculate(double[] input){
		setInput(input);
		calculate();
		return getOutput();
	}
	
	/**
	 * Performs calculation of all layers using current input
	 */
	public void calculate(){
		for(int i = 1; i<layers.length; i++){
			for(int j = 0; j<layers[i]; j++){
				inputs[i][j] = 0;
				
				for(int k = 0; k<layers[i-1]; k++)
					inputs[i][j] += weights[i][j][k] * outputs[i-1][k];
				
				inputs[i][j] += biases[i][j];
				
				outputs[i][j] = activationFunction(inputs[i][j]);
			}
		}
	}
	
	/**
	 * Performs calculations on network using given output (last layer) and returns result (first layer)
	 * @param output
	 * @return double[]
	 */
	public double[] reverseCalculate(double[] output){
		setOutput(output);
		reverseCalculate();
		return getInput();
	}
	
	/**
	 * Performs reverse calculation of all layers using current output
	 */
	public void reverseCalculate(){
		for(int i = layers.length-2; i>=0; i--){
			for(int j = 0; j<layers[i]; j++){
				inputs[i][j] = 0;
				
				for(int k = 0; k<layers[i+1]; k++)
					inputs[i][j] += weights[i+1][k][j] * outputs[i+1][k];
				
				inputs[i][j] += biases[i][j];
				
				outputs[i][j] = inputs[i][j];
				//outputs[i][j] = activationFunction(inputs[i][j]);
			}
		}
	}
	
	/**
	 * Changes weights and biases towards these when output generated for given input is equal to given expected values
	 * @param input
	 * @param expected
	 */
	public void teachOnce(double[] input, double[] expected){
		setInput(input);
		calculate();
		calculateErrors(expected);
		calculateGradients();
		performChanges();
	}
	
	private void calculateErrors(double[] expected){
		if(expected.length != layers[layers.length-1])
			throw new IllegalArgumentException("Length of expected output must be equal to number of neurons in last layer");
		
		// last layer number
		int layer = layers.length-1;
		
		// calculating error for last layer
		for(int i = 0; i<layers[layer]; i++){
			errors[layer][i] = (outputs[layer][i] - expected[i])*activationDerivative(inputs[layer][i]);
		}
		
		// calculating error for other layers
		while(--layer>0){
			for(int i = 0; i<layers[layer]; i++){
				double temp = 0;
				for(int j = 0; j<layers[layer+1]; j++)
					temp += errors[layer+1][j]*weights[layer+1][j][i];
				errors[layer][i] = temp*activationDerivative(inputs[layer][i]);
			}
		}
	}
	
	private void calculateGradients(){
		gradientBiases = errors;
		
		for(int i = 1; i<layers.length; i++)
			for(int j = 0; j<layers[i]; j++)
				for(int k = 0; k<layers[i-1]; k++)
					gradientWeights[i][j][k] = outputs[i-1][k]*errors[i][j];
	}
	
	private void performChanges(){
		for(int i = 1; i<layers.length; i++){
			for(int j = 0; j<layers[i]; j++){
				for(int k = 0; k<layers[i-1]; k++)
					weights[i][j][k] -= LEARNING_RATIO * gradientWeights[i][j][k];
				biases[i][j] -= LEARNING_RATIO * gradientBiases[i][j];
			}
		}
	}
	
	/**
	 * Sets input (first layer) to given values
	 * @param input
	 */
	public void setInput(double[] input){
		if(input.length != outputs[0].length)
			throw new IllegalArgumentException("Input length must be equal to size of first layer");
		outputs[0] = input;
	}
	
	/**
	 * Returns current input (outputs of first layer)
	 * @return double[]
	 */
	public double[] getInput(){
		return outputs[0];
	}
	
	/**
	 * Sets output (last layer) to given values
	 * @param output
	 */
	public void setOutput(double[] output){
		if(output.length != outputs[layers.length-1].length)
			throw new IllegalArgumentException("Output length must be equal to size of last layer");
		outputs[layers.length-1] = output;
	}
	
	/**
	 * Returns current output (outputs of last layer)
	 * @return double[]
	 */
	public double[] getOutput(){
		return outputs[layers.length-1];
	}
	
	protected double activationFunction(double a){
		return 1/(1 + Math.exp(-1*a));
	}
	
	protected double activationDerivative(double a){
		return Math.exp(a)/(Math.pow(Math.exp(a)+1 , 2));
	}

}
