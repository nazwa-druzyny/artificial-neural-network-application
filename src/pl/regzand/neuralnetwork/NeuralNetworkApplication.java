package pl.regzand.neuralnetwork;

import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import pl.regzand.neuralnetwork.views.MenuBar;
import pl.regzand.neuralnetwork.views.ReversedView;
import pl.regzand.neuralnetwork.views.TeachingView;
import pl.regzand.neuralnetwork.views.View;

public class NeuralNetworkApplication extends JFrame {
	
	public static NeuralNetworkApplication instance;
	
	public final MenuBar menuBar;
	public final CardLayout layout;
	public final List<View> views;
	
	private NeuralNetwork neuralNetwork;
	
	public NeuralNetworkApplication(){
		instance = this;
		
		// frame configuration
		super.setVisible(true);
		super.setTitle("Neural Network Visualization Tool");
		super.setSize(1000, 750);
		super.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		// views configuration
		views = new ArrayList<View>();
		views.add(new ReversedView(this));
		views.add(new TeachingView(this));
		
		// menu bar configuration
		menuBar = new MenuBar(this);
		super.setJMenuBar(menuBar);

		// layout configuration
		layout = new CardLayout(15, 15);
		super.setLayout(layout);
		for(View v : views)
			super.add(v, v.getName());
		
	}
	
	public boolean isLoaded(){
		return neuralNetwork != null;
	}
	
	public void setNeuralNetwork(NeuralNetwork net){
		neuralNetwork = net;
		this.triggerNetworkUpdate();
	}
	
	public void triggerNetworkUpdate(){
		this.menuBar.update();
		for(View v : views)
			v.onNetworkUpdate(neuralNetwork);
	}
	
	public NeuralNetwork getNeuralNetwork(){
		return neuralNetwork;
	}
}
