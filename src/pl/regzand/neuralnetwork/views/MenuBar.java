package pl.regzand.neuralnetwork.views;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

import pl.regzand.neuralnetwork.NeuralNetworkApplication;

public class MenuBar extends JMenuBar {
	
	private final NeuralNetworkApplication app;
	private final MenuListener listener;
	
	public final JMenu menuFile;
	public final JMenu menuView;

	public MenuBar(NeuralNetworkApplication app) {
		this.app = app;
		
		listener = new MenuListener(app);
		
		menuFile = new JMenu("File");
		menuView = new JMenu("View");
		
		setupFile();
		setupView();
		
		update();
	}
	
	public void update(){
		boolean isLoaded = app.isLoaded();
			
		menuFile.getItem(2).setEnabled(isLoaded);
		menuView.setEnabled(isLoaded);
	}
	
	private void setupFile(){
		super.add(menuFile);
		
		JMenuItem item;
		
		// File -> New
		item = new JMenuItem("New");
		item.setToolTipText("Creates new neural network");
		item.setActionCommand("FileNew");
		item.addActionListener(listener);
		menuFile.add(item);
		
		// File -> Import
		item = new JMenuItem("Import");
		item.setToolTipText("Imports existing neural network structure");
		item.setActionCommand("FileImport");
		item.addActionListener(listener);
		menuFile.add(item);
		
		// File -> Export
		item = new JMenuItem("Export");
		item.setToolTipText("Exports structure of actual neuron network");
		item.setActionCommand("FileExport");
		item.addActionListener(listener);
		menuFile.add(item);
		
		// File -> *separator*
		menuFile.addSeparator();
		
		// File -> Exit
		item = new JMenuItem("Exit");
		item.setToolTipText("Exit application");
		item.setActionCommand("FileExit");
		item.addActionListener(listener);
		menuFile.add(item);
		
	}
	
	private void setupView(){
		super.add(menuView);
		
		ButtonGroup group = new ButtonGroup();
		
		// load all views from app
		for(View v : app.views){
			JRadioButtonMenuItem item = new JRadioButtonMenuItem(v.getName());
			item.setToolTipText(v.getDescription());
			item.setActionCommand("View"+v.getName());
			item.addActionListener(listener);
			
			group.add(item);
			menuView.add(item);
		}
		
		// select first element by default
		group.getElements().nextElement().setSelected(true);
	}

}
