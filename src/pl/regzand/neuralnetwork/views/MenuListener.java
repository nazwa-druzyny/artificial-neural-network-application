package pl.regzand.neuralnetwork.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import pl.regzand.neuralnetwork.NeuralNetwork;
import pl.regzand.neuralnetwork.NeuralNetworkApplication;

public class MenuListener implements ActionListener {
	
	private final NeuralNetworkApplication app;
	
	public MenuListener(NeuralNetworkApplication app) {
		this.app = app;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Command: "+e.getActionCommand());
		
		if(e.getActionCommand().startsWith("View")){
			app.layout.show(app.getContentPane(), e.getActionCommand().substring(4));
			return;
		}
		
		switch(e.getActionCommand()){
		
			case "FileNew":
				create();
				break;
				
			case "FileImport":
				importFile();
				break;
				
			case "FileExport":
				exportFile();
				break;
				
			case "FileExit":
				System.exit(0);
				break;
				
			case "ViewStructure":
				break;
				
			case "ViewData":
				break;
				
			case "ViewTests":
				break;
			
			default:
				System.err.println("Unknown command: "+e.getActionCommand());
		}
	}
	
	private void create(){
		try{
			String in = JOptionPane.showInputDialog("Enter number of neurons in each layer (integers divided by spaces)");
			if(in==null)
				return;
			
			String[] input = in.split(" ");
			
			int[] layers = new int[input.length];
			for(int i = 0; i<input.length; i++){
				layers[i] = Integer.parseInt(input[i]);
			}
			
			app.setNeuralNetwork(new NeuralNetwork(layers));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void importFile(){
		JFileChooser fc = new JFileChooser();
		
		if(fc.showOpenDialog(null) != JFileChooser.APPROVE_OPTION)
			return;
		
		try {
			NeuralNetwork net = new NeuralNetwork(fc.getSelectedFile());
			app.setNeuralNetwork(net);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private void exportFile(){
		JFileChooser fc = new JFileChooser();
		
		if(fc.showSaveDialog(null) != JFileChooser.APPROVE_OPTION)
			return;
		
		try {
			app.getNeuralNetwork().saveToFile(fc.getSelectedFile());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
