package pl.regzand.neuralnetwork.views;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import pl.regzand.neuralnetwork.NeuralNetwork;
import pl.regzand.neuralnetwork.NeuralNetworkApplication;

public class ReversedView extends View {

	private final SpinnerNumberModel spinnerModel;
	private final GridPanel grid;
	
	public ReversedView(NeuralNetworkApplication app) {
		super("Reversed", "???", app);
		
		// setting panel layout
		GroupLayout layout = new GroupLayout(this);
		super.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		// creating spinner
		spinnerModel = new SpinnerNumberModel(0, 0, 0, 1);
		JSpinner spinner = new JSpinner(spinnerModel);
		spinner.setMaximumSize(new Dimension(100, 35));
		
		// creating grid
		JPanel display = new JPanel();
		display.setBorder(new EmptyBorder(30, 30, 30, 30));
		grid = new GridPanel(app, spinnerModel);
		display.add(grid);
		
		// setting layout
		layout.setHorizontalGroup(layout.createSequentialGroup()
			.addComponent(display)
			.addGroup(layout.createParallelGroup()
				.addComponent(spinner)
			)
		);
		layout.setVerticalGroup(layout.createSequentialGroup()
			.addGroup(layout.createParallelGroup()
				.addComponent(display)
				.addGroup(layout.createSequentialGroup()
					.addComponent(spinner)
				)
			)	
		);
		

		spinnerModel.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				grid.validate();
				grid.repaint();
			}
		});
		
	}

	@Override
	public void onNetworkUpdate(NeuralNetwork net) {
		spinnerModel.setMaximum(net.layers[net.layers.length-1]-1);
	}

	private class GridPanel extends JPanel {
		
		private final NeuralNetworkApplication app;
		private final SpinnerNumberModel spinnerModel;
		
		public GridPanel(NeuralNetworkApplication app, SpinnerNumberModel spinneModel){
			this.app = app;
			this.spinnerModel = spinneModel;
		}
		
		@Override
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			
			// if neural network isn't loaded we don't have anything to display
			if(!app.isLoaded())
				return;
			
			// setting up graphics
			Graphics2D g2 = (Graphics2D)g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			
			// getting neural network
			NeuralNetwork net = app.getNeuralNetwork();
			
			// getting data form net
			double[] output = new double[net.layers[net.layers.length-1]];
			output[spinnerModel.getNumber().intValue()] = 1;
			double[] input = net.reverseCalculate(output);
			
			// getting sizes of grid
			int gridSize = (int)Math.floor(Math.sqrt(input.length));
			int cellSize = (super.getWidth()-60)/gridSize;
			
			// getting max absolute value
			double max = 0;
			for(double d : input)
				if(Math.abs(d)>max)
					max = Math.abs(d);
			
			// creating grid
			for(int i = 0; i<gridSize*gridSize; i++){
				
				g2.setColor(getColor(input[i], max));
				g2.fillRect((i%gridSize)*cellSize, ((int)(i/gridSize))*cellSize, cellSize, cellSize);
				
			}
			
			// creating border
			int s = gridSize*cellSize;
			g2.setColor(Color.gray);
			g2.setStroke(new BasicStroke(2));
			g2.drawLine(0, 0, s, 0);
			g2.drawLine(s, 0, s, s);
			g2.drawLine(s, s, 0, s);
			g2.drawLine(0, s, 0, 0);
		}
		
		private Color getColor(double x, double max){
			int a = (int)((Math.abs(x)/max)*255);
			
			if(x>0)
				return new Color(0, 255, 0, a);
			else
				return new Color(255, 0, 0, a);
		}
		
		@Override
		public Dimension getPreferredSize(){
			Dimension parent = super.getParent().getSize();
			int size = (int) Math.min(parent.getWidth(), parent.getHeight());
			return new Dimension(size, size);
		}
	}
	
}
