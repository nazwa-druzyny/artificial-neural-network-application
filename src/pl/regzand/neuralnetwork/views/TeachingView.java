package pl.regzand.neuralnetwork.views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

import pl.regzand.neuralnetwork.NeuralNetwork;
import pl.regzand.neuralnetwork.NeuralNetworkApplication;

public class TeachingView extends View {

	public TeachingView(final NeuralNetworkApplication app) {
		super("Teaching", "Interface for teaching neural network", app);
		
		// setting panel layout
		GroupLayout layout = new GroupLayout(this);
		super.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		// creating buttons
		JButton button = new JButton("Load tasks");
		
		// creating display
		JPanel display = new JPanel();
		
		// setting layout
		layout.setHorizontalGroup(layout.createSequentialGroup()
			.addComponent(display)
			.addGroup(layout.createParallelGroup()
				.addComponent(button)
			)
		);
		layout.setVerticalGroup(layout.createSequentialGroup()
			.addGroup(layout.createParallelGroup()
				.addComponent(display)
				.addGroup(layout.createSequentialGroup()
					.addComponent(button)
				)
			)	
		);
		
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				if(!NeuralNetworkApplication.instance.isLoaded())
					return;
				
				JFileChooser fc = new JFileChooser();
				fc.setMultiSelectionEnabled(true);
				
				// showing dialog to choose files
				if(fc.showOpenDialog(null) != JFileChooser.APPROVE_OPTION)
					return;
				
				// getting selected files
				File[] files = fc.getSelectedFiles();
				
				// getting neural network
				NeuralNetwork net = NeuralNetworkApplication.instance.getNeuralNetwork();
				
				// getting count of neurons in input and output layers
				int firstLayer = net.layers[0];
				int lastLayer = net.layers[net.layers.length-1];
				
				// loading data
				List<double[]> inputs = new ArrayList<double[]>();
				List<double[]> expected = new ArrayList<double[]>();
				for(int i = 0; i<files.length; i++){
					try{
						double[] ex = new double[lastLayer];
						double[] in = new double[firstLayer];
						
						// selecting expected output
						ex[Integer.parseInt(files[i].getName().split("_")[0])] = 1;
						
						// loading file
						Scanner file = new Scanner(files[i]);
						
						// skipping unnecessary lines
						file.nextLine();
						file.nextLine();
						file.nextLine();
						
						// loading inputs
						for(int j = 0; j<firstLayer; j++)
							in[j] = file.nextDouble();
						
						// no exceptions => adding this data
						inputs.add(in);
						expected.add(ex);
						
						file.close();
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
				// teaching
				net.teach(inputs.toArray(new double[inputs.size()][]), expected.toArray(new double[expected.size()][]));
			}
		});
		
	}

}
