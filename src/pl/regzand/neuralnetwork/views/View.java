package pl.regzand.neuralnetwork.views;

import javax.swing.JPanel;

import pl.regzand.neuralnetwork.NeuralNetwork;
import pl.regzand.neuralnetwork.NeuralNetworkApplication;

public abstract class View extends JPanel {
	
	protected final NeuralNetworkApplication app;
	protected final String name;
	protected final String description;
	
	public View(String name, String description, NeuralNetworkApplication app){
		this.name = name;
		this.app = app;
		this.description = description;
	}
	
	public void onNetworkUpdate(NeuralNetwork net){};
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}

}
